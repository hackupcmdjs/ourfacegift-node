var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app).listen(3000);
var io = require('socket.io').listen(server);



var pug = require('pug')
var path = require('path')
var uuid = require('uuid');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Pdf = require('./pdfs.model.js')
var fs = require('fs');
var phantom = require('phantom');
var _ = require('lodash');
var rp = require('request-promise');






mongoose.connect('mongodb://localhost/photogift');

app.get('/friendslist', function(req, res) {
    var token = req.query.access_token;
    var options = {
        uri: 'https://graph.facebook.com/v2.8/me/photos?type=tagged&fields=tags&limit=10000',
        qs: {
            access_token: token
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function(response) {
            var hash = {}
            var arrayResult = []
            _.map(response.data, (data) => {
                _.map(data.tags.data, (data) => {
                    if (!hash.hasOwnProperty(data.id)) {
                        var obj = {
                            id: data.id,
                            name: data.name
                        };
                        arrayResult.push(obj);
                        hash[data.id] = obj;
                    }
                })
            });
            res.json(_.sortBy(arrayResult,function(a){return a.name}))
        })
        .catch(function(err) {
            if(err) console.log('Err , failed ', JSON.stringify(err));
        });
});


app.get('/', function(req, res) {
    res.send('Hello World!');
});

app.get('/collage/:uid', function(req, res) {
    Pdf.findOne({
        uid: req.params.uid
    }).exec((err, doc) => {
        if(err || !doc) return res.status(500).send();
        return res.sendFile(path.join(doc.path));
    })
});


function getPhotosWithUser(token, userId, cb) {
    var url = "https://graph.facebook.com/v2.8/me/photos?type=tagged&limit=10000&fields=tags{id},id"
    var options = {
        uri: url,
        qs: {
            access_token: token
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function(response) {
            var filtered = [];
            _.forEach(response.data, (photo) => {
                var obj = _.find(photo.tags.data, (tag) => {
                    return tag.id == userId;
                })
                if(obj) filtered.push(photo.id);
            })
            cb(filtered)
        })
        .catch(function(err) {
            if(err) console.log('Err , failed ' + JSON.stringify(err));
        });
}

  function getPhotosUrl(photosIds, token, cb) {
    var promises = [];
    _.forEach(photosIds,function(id){
      var url = "https://graph.facebook.com/v2.8/"+id+"?fields=images{source}"
      var options = {
          uri: url,
          qs: {
              access_token: token
          },
          headers: {
              'User-Agent': 'Request-Promise'
          },
          json: true // Automatically parses the JSON string in the response
      };
      // console.log('Pushing option');
      promises.push(rp(options))
    })

    Promise.all(promises).then((values) => {
      // console.log('Values ', values);
      values = _.map(values,(o) => {
        return o.images[0].source;
      })
      // console.log('Mapped values', values);
      cb(values);
    },(err) => {console.log('Error ' , JSON.stringify(err));})
}



app.get('/pdf', function(req, res) {
    var uid = uuid.v1();
    // console.log('UID ', uid);
    var token = req.query.access_token;
    var userId = req.query.userId;
    var sources = [];
    getPhotosWithUser(token, userId, function(result) {
        // console.log('Results ids ' , result);
        getPhotosUrl(result,token,function(s){
            sources = _.shuffle(s);

            // var width = 1191;
            // var imgWidth = width / 4;
            // var height = 842;
            // var imgHeight = 842/4;

            var html = pug.renderFile("template.pug", {
                sources: sources,
                // imgWidth: imgWidth,
                // imgHeight: imgHeight,
                // width: width
            })

            var htmlFileName = 'collage_'+uid+'.html'
            fs.writeFile(htmlFileName, html, function(err) {
                if (err) return console.log(err);

                // var options = {
                //   // "html" : "Path to HTML file",
                //   // "css" : "Path to additional CSS file",
                //   // "js" : "Path to additional JavaScript file",
                //   // "runnings" : "Path to runnings file. Check further below for explanation.",
                //   "paperSize":{format: 'A4', orientation: 'portrait', border: '1cm'}
                //   // "deleteOnAction" : true/false (Deletes the created temp file once you access it via toBuffer() or toFile())
                // }
                phantom.create().then(function(ph) {
                    ph.createPage().then(function(page) {
                        page.property('paperSize', {
                            format: 'A3',
                            orientation: 'landscape',
                            css : "/styles.css",
                        }).then(function() {
                            page.open(htmlFileName).then(function(status) {
                                var filename = "collage_" + uid + ".pdf"
                                page.render(filename).then(function() {
                                    ph.exit();
                                    var pdfEntity = new Pdf({
                                      path: path.join(process.cwd(),filename),
                                      uid:uid
                                    })
                                    pdfEntity.save((err,doc)=>{
                                      if(err){
                                        return console.log('error saving entity ' + err.message);
                                      }
                                      io.emit('pdfGenerated', {uid:uid});
                                      fs.unlink(htmlFileName,(err,data) => {
                                        if (err) return console.log('Error erasing');
                                        console.log('Erased html file');
                                      })
                                    })
                                });
                            });
                        })
                    });
                });
            });
        });
    });
    res.json({uid:uid});
});


io.on('connection', function (socket) {
  socket.on('my other event', function (data) {
    console.log(data);
  });
  socket.on('test', function (data) {
    console.log("test connection ok");
  });

});


// app.listen(3000, function() {
//     console.log('Example app listening on port 3000!');
// });
