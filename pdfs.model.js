var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var pdfSchema = new Schema({
  path: String,
  uid: String
});

// the schema is useless so far
// we need to create a model using it
var Pdf = mongoose.model('Pdf', pdfSchema);

// make this available to our users in our Node applications
module.exports = Pdf;
